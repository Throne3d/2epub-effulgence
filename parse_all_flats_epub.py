#!/usr/bin/python3
# coding: utf-8
from bs4 import BeautifulSoup
from ebooklib import epub
import re
import urllib
import urllib.parse
import subprocess
import os
import sys
import math

def findFileFromURL(chapterURL, where="web_cache"):
    if (where != ""): where = where + "/"
    originalChapterURL = chapterURL
    chapterURL = chapterURL.replace("http://", where).replace("https://", where)
    
    urlSplit = urllib.parse.urlsplit(chapterURL)
    queryParams = urllib.parse.parse_qs(urlSplit.query)
    
    fileName = urlSplit.path.split("/")[-1]
    filePath = ("/").join(urlSplit.path.split("/")[:-1]) + "/"
    
    #print("Looking for: {}".format(chapterURL))
    
    foundFile = None # e.g. "513.html?blah=blah" when found
    if (not os.path.isdir(filePath)):
        print("Couldn't find the directory [{}].".format(filePath))
        print("Chapter URL: {}".format(originalChapterURL))
        print("Where: {}".format(where))
    else:
        for item in os.listdir(filePath):
            itemSplit = urllib.parse.urlsplit(item)
            
            if (itemSplit.path == fileName and os.path.isfile(filePath + item)):
                #File has our filename, it's also a file.
                fileParams = urllib.parse.parse_qs(itemSplit.query)
                hasParams = True
                #print("File: {}".format(item))
                for queryParam in queryParams:
                    if (not queryParam in fileParams or not (queryParams[queryParam] in fileParams[queryParam] or fileParams[queryParam] == queryParams[queryParam])):
                        #It doesn't have the current param
                        hasParams = False
                        #print("File is missing param {} or fileParams[queryParam] ({}) doesn't have queryParams[queryParam] ({})".format(queryParam, fileParams[queryParam] if queryParam in fileParams else "Missing", queryParams[queryParam]))
                for fileParam in fileParams:
                    for fileParamValue in fileParams[fileParam]:
                        if (not fileParam in queryParams or not (fileParamValue in queryParams[fileParam] or queryParams[fileParam] == fileParamValue)):
                            hasParams = False #Some parameters are missing from the input for this to be the valid file.
                            #print("File has a param ({}) not in query - either queryParams[fileParam] ({}) is missing or not the same as the file's ({})".format(fileParam, queryParams[fileParam] if fileParam in queryParams else "Missing", fileParamValue))
                if (hasParams):
                    if (foundFile):
                        if (os.path.getmtime(foundFile) < os.path.getmtime(filePath + item)):
                            #The new file is more recent - ignore the previous
                            foundFile = filePath + item
                    else:
                        foundFile = filePath + item
        
    #print("Number of files in {}: {}".format(filePath, len(os.listdir(filePath))))
    if not (foundFile):
        print("Couldn't find file [{}] in [{}] (findFileFromURL(\"{}\", \"{}\"))".format(fileName, filePath, originalChapterURL, where))
        #raise FileNotFoundError("Couldn't find file [{}]".format(chapterURL))
        return None
    else:
        return foundFile

def set_param_in_url(url, *args, **kargs):
    if (not len(args) % 2 == 0):
        raise ValueError("Please give a URL then index,value pairs to replace in the query string.")
    
    urlSplit = urllib.parse.urlsplit(url)
    params = urllib.parse.parse_qs(urlSplit.query)
    
    for i in range(0, int(len(args)/2)):
        params[args[i * 2]] = [args[i * 2 + 1]]
    for karg in kargs:
        params[karg] = [kargs[karg]]
    
    new_urlSplit = urlSplit._replace(query=urllib.parse.urlencode(params, doseq=True))
    return urllib.parse.urlunsplit(new_urlSplit)

def clear_params_in_url(url):
    urlSplit = urllib.parse.urlsplit(url)
    new_urlSplit = urlSplit._replace(query=[])
    return urllib.parse.urlunsplit(new_urlSplit)
    
def set_url_params(url, *args, **kargs):
    return set_param_in_url(clear_params_in_url(url), *args, **kargs)

images = []

#Fetch a file from [url] to the directory [where="web_cache"], showing output if [output],
#generating a message if [notify], and waiting for the download to finish if [wait].
def get_file(url, where="web_cache", output=False, notify=False, wait=True, timestamping=True):
    if (not os.path.isdir(where)):
        os.makedirs(where)
    proc = subprocess.Popen("cd \"" + where + "\" && wget --load-cookies ../wget-cookies --save-cookies ../wget-cookies -w 0.5 --no-verbose --force-directories" + (" --timestamping" if timestamping else "") +" \"" + url + "\"" + (" > /dev/null 2>&1" if not output else ""), shell=True)
    if (wait): 
        proc.wait()
        if (not findFileFromURL(url, where=where)):
            #Retry
            proc = subprocess.Popen("cd \"" + where + "\" && wget --load-cookies ../wget-cookies --save-cookies ../wget-cookies -w 0.5 --force-directories" + (" --timestamping" if timestamping else "") +" \"" + url + "\"", shell=True)
            proc.wait()
            if (not findFileFromURL(url, where=where)):
                raise FileNotFoundError("Couldn't download file [{}]".format(url))
    if (notify): print("Got file {}".format(url))

def get_file_for_output(url):
    get_file(url, where="output")

def remove_control_characters(html):
    def str_to_int(s, default, base=10):
        if int(s, base) < 0x10000:
            return unichr(int(s, base))
        return default
    html = re.sub(r"&#(\d+);?", lambda c: str_to_int(c.group(1), c.group(0)), html)
    html = re.sub(r"&#[xX]([0-9a-fA-F]+);?", lambda c: str_to_int(c.group(1), c.group(0), base=16), html)
    html = re.sub(r"[\x00-\x08\x0b\x0e-\x1f\x7f]", "", html)
    return html
    
book = epub.EpubBook()
book.set_identifier("effulgence_epub")
book.set_title("Effulgence")
book.set_language("en")

book.add_author("Alicorn", uid="alicorn")
book.add_author("Kappa", uid="kappa")

style = '''
@namespace epub "http://www.idpf.org/2007/ops";
h3.entry-title {
    border-bottom: solid 1px #aaa;
    padding-bottom: 5px;
}
body {
    font-family: sans-serif;
}
div[data-type] {
    padding-top: 10px;
}
.user {
    border: solid 1px #aaa;
    padding: 10px;
    background-color: #eee;

    break-inside: avoid-column;
    -webkit-column-break-inside: avoid;
    -moz-column-break-inside: avoid;
    -o-column-break-inside: avoid;
    column-break-inside: avoid;
    page-break-inside: avoid;

    break-after: avoid-column;
    -webkit-column-break-after: avoid;
    -moz-column-break-after: avoid;
    -o-column-break-after: avoid;
    column-break-after: avoid;
    page-break-after: avoid;

    display: block;
}
.kappa .user {
    background-color: #dfcfef;
}
.alicorn .user {
    background-color: #bfffeb;
}
.aestrix .user {
    background-color: #bfdfff;
}
.usericon {
    padding: 0;
    margin: 0;
    display: table-cell;
    vertical-align: middle;
    max-height: 102px;
    max-width: 102px;
    width: 4em;
}
.usericon img, .usericon div {
    display: table-cell;
    border: solid 1px #aaa;
    max-height: 102px;
    max-width: 102px;
    height: 4em;
    width: 4em;
}
.username {
    display: table-cell;
    padding: 10px 10px 10px 15px;
    vertical-align: bottom;
    font-size: 90%;
    font-weight: 600;
}
.content {
    padding-top: 5px;
    page-break-before: avoid;
}
.branchNote {
    color: #c50;
    font-weight: bold;
    border-top: solid 1px #ddd;
    padding-top: 5px;
    padding-bottom: 5px;
}
.contMsg {
    color: #c50;
    font-weight: bold;
    border-top: solid 1px #ddd;
    padding-top: 5px;
    padding-bottom: 5px;
    margin-top: 20px;

    break-inside: avoid-column;
    -webkit-column-break-inside: avoid;
    -moz-column-break-inside: avoid;
    -o-column-break-inside: avoid;
    column-break-inside: avoid;
    page-break-inside: avoid;

    break-after: avoid-column;
    -webkit-column-break-after: avoid;
    -moz-column-break-after: avoid;
    -o-column-break-after: avoid;
    column-break-after: avoid;
    page-break-after: avoid;
}
.edittime {
    font-size: 80%;
}
'''

css = epub.EpubItem(uid="style_default", file_name="style/default.css", media_type="text/css", content=style)
book.add_item(css)

prevMajorTitle = ""

toc = []
prevCategory = []
spine = ["nav"]

if (not os.path.isfile("web_cache/chapterList.txt") or not os.path.isfile("web_cache/chapterNames.txt")):
    print("There is no list of chapters.")
    sys.exit(1)
    
collectionList = {}
collectionCache = {}
with open("web_cache/collectionPages.txt", mode="r") as collections:
    for collection in collections:
        collection = collection.replace("\n", "")
        if (collection.strip() != "" and not collection.find(" ~#~ ") == -1):
            #Line should exist
            collectionName = collection.split(" ~#~ ")[0].strip()
            collectionURL = collection.split(" ~#~ ")[1].strip()
            
            print("Processing collection {}.".format(collectionName))
            
            get_file(collectionURL)
            collectionPage = findFileFromURL(collectionURL)
            
            collectionSoup = BeautifulSoup(open(collectionPage))
            
            collectionList[collectionName] = []
            
            #Each collection will have a class of "collectionnametolower" - e.g. "Alicorn ~#~ http://belltower.dreamwidth.org/profile" will make any members have the class "alicorn" (CSS selected by ".alicorn")
            #This will be done later
            
            #Dreamwidth specific
            members = collectionSoup.select("div#members_people_body a")
            for member in members:
                memberURL = member["href"].replace("https://", "http://").replace("/profile", "/").strip()
                if (memberURL != ""):
                    collectionList[collectionName].append(memberURL)
            
            print("Found {} members.".format(len(collectionList[collectionName])))

with open("web_cache/chapterList.txt", mode="r") as f:
    with open("web_cache/chapterNames.txt", mode="r") as n:
        for line in f:
            if (line != ""):
                chapterNameLine = n.readline().replace("\n", "")
                lineURL = line.replace("\n", "")
                file_name = lineURL.replace("http://", "web_cache/").replace("https://", "web_cache/")
                page_count = 1
                
                print("Adding chapter: {}".format(chapterNameLine))
                
                with open(findFileFromURL(lineURL)) as first_flat_file:
                    #print("URL: {}".format(findFileFromURL(lineURL)))
                    soup = BeautifulSoup(first_flat_file)
                    page_list = soup.select("div.comment-page-list")
                    if (page_list):
                        text = page_list[0].find("p")
                        #print("PageList p: {}".format(text))
                        page_count = int(re.match(r'Page 1 of ([0-9]+)', page_list[0].find("p").text).groups()[0])
                    #print("{}: {} pages".format(soup.select("h3.entry-title")[0].text, str(page_count)))
                    
                    sectionName = chapterNameLine.split(" ~#~ ")[0]
                    chapterName = chapterNameLine.split(" ~#~ ")[1]
                    
                    newPageData = BeautifulSoup("<html><head></head><body></body></html>")
                    
                    entry = soup.select("div.entry")[0]
                    entry_userPic = soup.select("div.entry")[0].select("div.userpic")[0]
                
                    if (prevMajorTitle != sectionName):
                        prevMajorTitle = sectionName
                        
                        if (len(prevCategory) > 1):
                            toc.append(prevCategory)
                        prevCategory = [epub.Section(prevMajorTitle), []]
                        
                        majorTitleStuff = newPageData.new_tag("h2")
                        majorTitleStuff["class"] = "section-title"
                        majorTitleStuff.append("{}".format(prevMajorTitle))
                        newPageData.find("body").append(majorTitleStuff)
                    
                    titleStuff = newPageData.new_tag("h3")
                    titleStuff["class"] = "entry-title"
                    titleStuff.append(soup.select("h3.entry-title")[0].text)
                    newPageData.find("body").append(titleStuff)
                    
                    htmlStuff = newPageData.new_tag("div", id="cmt-0", depth=0)
                    htmlStuff["data-type"] = "entry"
                    
                    #Moiety - Dreamwidth, incandescence & effulgence, specific
                    posterURLs = entry.select("span.poster a[href]")
                    userURL = ""
                    for posterURL in posterURLs:
                        if (posterURL["href"].strip() != ""):
                            userURL = posterURL["href"].replace("https://", "http://").replace("/profile", "/")
                            break
                    
                    if (userURL.strip() in collectionCache):
                        htmlStuff["class"] = collectionCache[userURL]
                    else:
                        htmlStuff["class"] = ""
                        if (userURL != ""):
                            for collectionName in collectionList:
                                collection = collectionList[collectionName]
                                for memberURL in collection:
                                    if (memberURL.strip() == userURL.strip()):
                                        htmlStuff["class"] += re.sub("[^A-Za-z0-9_]", "", collectionName.lower()) + " "
                        
                        htmlStuff["class"] = htmlStuff["class"].strip()
                        if (not "class" in htmlStuff.attrs or htmlStuff["class"].strip() == ""):
                            htmlStuff["class"] = "nocollection"
                            print("Couldn't find a collection for \"{}\".".format(userURL))
                        else:
                            print("Found a collection for \"{}\"! (\"{}\")".format(userURL, htmlStuff["class"]))
                        
                        collectionCache[userURL] = htmlStuff["class"]
                    
                    
                    newUserDiv = newPageData.new_tag("div")
                    newUserDiv["class"] = "user"
                    
                    newUserIconDiv = newPageData.new_tag("div")
                    newUserIconDiv["class"] = "usericon"
                    
                    if (entry_userPic.a and entry_userPic.a["href"] != "" and entry_userPic.find("img") and entry_userPic.find("img")["src"]):
                        username = BeautifulSoup(entry_userPic.find("img")["title"]).p.text
                        entry_userPic.a["href"] = set_param_in_url(entry_userPic.a["href"], "style", "site")
                        
                        iconURL = entry_userPic.find("img")["src"].strip().replace("https://", "http://")
                        if (not iconURL in images):
                            images.append(iconURL)
                            get_file_for_output(iconURL)
                            
                            internal_URL = iconURL.replace("http://", "output/")
                            img = epub.EpubItem(uid=internal_URL.replace("output/", "images/").replace("/", "-"), file_name=internal_URL.replace("output/", "images/") + ".png", media_type="image/png")
                            with open(internal_URL, mode="rb") as imag:
                                img.set_content(imag.read())
                            book.add_item(img)
                            
                        newUserIcon = newPageData.new_tag("img")
                        newUserIcon["src"] = iconURL.replace("http://", "images/") + ".png"
                    else:
                        newUserIcon = newPageData.new_tag("div")
                        newUserIcon.append("X")
                        username = entry.select("span.ljuser")
                        if (username):
                            username = username[0]["lj:user"].strip()
                    
                    newUserIconDiv.append(newUserIcon)
                    newUserDiv.append(newUserIconDiv)
                    
                    newUsernameDiv = newPageData.new_tag("div")
                    newUsernameDiv["class"] = "username"
                    newUsernameDiv.append(username)
                    
                    newUserDiv.append(newUsernameDiv)
                    htmlStuff.append(newUserDiv)
                    
                    newContentDiv = newPageData.new_tag("div")
                    newContentDiv["class"] = "content"
                    newContentDiv.append(soup.select("div.entry-content")[0])
                    htmlStuff.append(newContentDiv)
                    
                    newPageData.find("body").append(htmlStuff)
                    
                    childThreads = {}
                    
                    prevID = 0
                    prevDepth = 0
                    prevDiv = htmlStuff
                    
                    for pageNum in range(1, page_count+1):
                        currChapterURL = findFileFromURL(set_url_params(lineURL, **{"view": "flat", "style": "site", "page" : pageNum}))
                        with open(currChapterURL, mode="r") as currPage:
                            currPageSoup = BeautifulSoup(currPage)
                            
                            comments = currPageSoup.select("div.comment")
                            if (len(comments) < 1):
                                print("This entry has no comments.")
                                if (page_count > 1):
                                    print("There's an issue with the page of URL {}. The entry has no comments, but the number of flat pages is greater than 1.".format(currChapterURL))
                            
                            for comment in comments:
                                id = int(comment["id"].split("comment-cmt")[1])
                                text = str(comment.select("div.comment-content")[0])
                                comment_userPic = comment.select("div.userpic")[0]
                                
                                parentLink = comment.select("li.commentparent")
                                parentNum = 0
                                if (parentLink):
                                    parentNum = int(parentLink[0].a["href"].split("#cmt")[1])
                                
                                depth = 0
                                
                                if (parentNum == prevID):
                                    depth = prevDepth + 1
                                    parentDiv = prevDiv
                                else:
                                    parentDiv = newPageData.select("div#cmt-{}".format(str(parentNum)))
                                    if (parentDiv):
                                        parentDiv = parentDiv[0]
                                        depth = parentDiv["depth"] + 1
                                    else:
                                        print("No div with id cmt-{} ? (Selector: div#cmt-{})".format(str(parentNum), str(parentNum)))
                                        print("We're on pageNum {} which has the URL {} (page {} of {} from {})".format(pageNum, currChapterURL, pageNum, page_count, chapterName))
                                        print("Curr DOM: {}".format(newPageData))
                                        print("Unknown depth, too. Assuming 1.")
                                        depth = 1
                                
                                htmlStuff = newPageData.new_tag("div", id="cmt-{}".format(id), parent=str(parentNum), depth = depth)
                                htmlStuff["data-type"] = "comment"
                                
                                #Moiety - Dreamwidth, incandescence & effulgence, specific
                                posterURLs = comment.select("span.poster a[href]")
                                userURL = ""
                                for posterURL in posterURLs:
                                    if (posterURL["href"].strip() != ""):
                                        userURL = posterURL["href"].replace("https://", "http://").replace("/profile", "/")
                                        break
                                
                                if (userURL.strip() in collectionCache):
                                    htmlStuff["class"] = collectionCache[userURL]
                                else:
                                    htmlStuff["class"] = ""
                                    if (userURL != ""):
                                        for collectionName in collectionList:
                                            collection = collectionList[collectionName]
                                            for memberURL in collection:
                                                if (memberURL.strip() == userURL.strip()):
                                                    htmlStuff["class"] += re.sub("[^A-Za-z0-9_]", "", collectionName.lower()) + " "
                                    
                                    htmlStuff["class"] = htmlStuff["class"].strip()
                                    if (not "class" in htmlStuff.attrs or htmlStuff["class"].strip() == ""):
                                        htmlStuff["class"] = "nocollection"
                                        print("Couldn't find a collection for \"{}\".".format(userURL))
                                    else:
                                        print("Found a collection for \"{}\"! (\"{}\")".format(userURL, htmlStuff["class"]))
                                    
                                    collectionCache[userURL] = htmlStuff["class"]
                                
                                
                                newUserDiv = newPageData.new_tag("div")
                                newUserDiv["class"] = "user"
                                
                                newUserIconDiv = newPageData.new_tag("div")
                                newUserIconDiv["class"] = "usericon"
                                
                                if (comment_userPic.a and comment_userPic.a["href"] != "" and comment_userPic.find("img") and comment_userPic.find("img")["src"]):
                                    username = BeautifulSoup(comment_userPic.find("img")["title"]).p.text
                                    comment_userPic.a["href"] = set_param_in_url(comment_userPic.a["href"], "style", "site")
                                    
                                    iconURL = comment_userPic.find("img")["src"].strip().replace("https://", "http://")
                                    if (not iconURL in images):
                                        images.append(iconURL)
                                        get_file_for_output(iconURL)
                                        
                                        internal_URL = iconURL.replace("http://", "output/")
                                        img = epub.EpubItem(uid=internal_URL.replace("output/", "images/").replace("/", "-"), file_name=internal_URL.replace("output/", "images/") + ".png", media_type="image/png")
                                        with open(internal_URL, mode="rb") as imag:
                                            img.set_content(imag.read())
                                        book.add_item(img)
                                        
                                    newUserIcon = newPageData.new_tag("img")
                                    newUserIcon["src"] = iconURL.replace("http://", "images/") + ".png"
                                else:
                                    newUserIcon = newPageData.new_tag("div")
                                    newUserIcon.append("X")
                                    username = comment.select("span.ljuser")
                                    if (username):
                                        username = username[0]["lj:user"].strip()
                                
                                newUserIconDiv.append(newUserIcon)
                                newUserDiv.append(newUserIconDiv)
                                
                                newUsernameDiv = newPageData.new_tag("div")
                                newUsernameDiv["class"] = "username"
                                newUsernameDiv.append(username)
                                
                                newUserDiv.append(newUsernameDiv)
                                htmlStuff.append(newUserDiv)
                                
                                newContentDiv = newPageData.new_tag("div")
                                newContentDiv["class"] = "content"
                                newContentDiv.append(comment.select("div.comment-content")[0])
                                htmlStuff.append(newContentDiv)
                                
                                parentDiv.insert_after(htmlStuff)
                                #print("I put the regular div [id='{}'] after its parent [id='{}']".format("cmt-{}".format(id), parentDiv["id"]))
                                
                                prevID = id
                                prevDepth = depth
                                prevDiv = htmlStuff
                                
                                if (parentNum in childThreads):
                                    childThreads[parentNum] = childThreads[parentNum] + 1
                                    htmlStuff["siblingNum"] = childThreads[parentNum]
                                else:
                                    childThreads[parentNum] = 1
                                
                                if (childThreads[parentNum] > 1):
                                    siblingDivs = newPageData.select("div[parent={}]".format(parentNum))
                                    
                                    highestSibling = {"siblingNum":0}
                                    for siblingDiv in siblingDivs:
                                        if (not ("siblingNum" in siblingDiv.attrs)):
                                            siblingDiv["siblingNum"] = 1
                                        if (int(siblingDiv["siblingNum"]) > int(highestSibling["siblingNum"])):
                                            highestSibling = siblingDiv
                                    for siblingDiv in siblingDivs:
                                        if (siblingDiv["siblingNum"] == highestSibling["siblingNum"] - 1):
                                            secondToLastSibling = siblingDiv
                                    if (len(siblingDivs) > 1):
                                        print("Found a branch.")
                                        secondToLastSiblingID = secondToLastSibling["id"].split("cmt-")[1]
                                        lastChild = secondToLastSibling
                                        lastParentID = secondToLastSiblingID
                                        lastParentChildren = newPageData.select("div[parent={}]".format(lastParentID))
                                        while (len(lastParentChildren) > 0): 
                                            #Any way to optimise this? D:
                                            lastChild = lastParentChildren[len(lastParentChildren)-1]
                                            lastParentID = lastChild["id"].split("cmt-")[1]
                                            lastParentChildren = newPageData.select("div[parent={}]".format(lastParentID))
                                            
                                        lastChild.insert_after(htmlStuff)
                                        #print("I put the new sibling div [id='{}'] after the last child [id='{}'] of the previous sibling div [id='{}']".format("cmt-{}".format(id), lastParentID, secondToLastSiblingID))
                                        if (len(siblingDivs) == 2):
                                            siblings = []
                                            for siblingDiv in siblingDivs:
                                                siblings.append(siblingDiv)
                                            siblings = sorted(siblings, key=lambda sibling: sibling["siblingNum"])
                                            
                                            branchNote = newPageData.new_tag("div")
                                            branchNote["class"] = "branchNote branchNote1"
                                            branchNote.append("This is a branch point. Multiple story threads start here. This is the first thread.")
                                            siblings[0].insert(0, branchNote)
                                            
                                            branchNote2 = newPageData.new_tag("div")
                                            branchNote2["class"] = "branchNote branchNote2"
                                            branchNote2.append("The previous branch has ended. This is thread #2.")
                                            htmlStuff.insert(0, branchNote2)
                                        else:
                                            branchNote = newPageData.new_tag("div")
                                            branchNote["class"] = "branchNote branchNote{}".format(len(siblingDivs))
                                            branchNote.append("The previous branch has ended. This is thread #{}.".format(len(siblingDivs)))
                                            htmlStuff.insert(0, branchNote)
                                    else:
                                        print("But apparently we don't? Number of things with {} as their parent: {}".format(parentNum, len(siblingDivs)))
                    
                    size = 60*1024
                    if (len(str(newPageData)) > size*1.3):
                        #Large HTML file (greater than 50KB?)
                        htmlData = str(newPageData.body)
                        split_count = 1
                        while (htmlData.strip() != ""):
                            
                            if (len(htmlData) <= size * 1.3):
                                actualPart = htmlData
                                index = len(htmlData)
                            else:
                                index1 = htmlData[:size].rfind("data-type=\"comment\"")
                                index = htmlData[:index1].rfind("<div") #Before "data-type='comment'", find "<div"
                                
                                actualPart = htmlData[:index] + "<div class='contMsg'>This chapter is continued on the next page.</div>"
                            pagePart = BeautifulSoup("<html><head></head><body>" + actualPart + "</body></html>")
                            
                            
                            fileName = re.sub("[^A-Za-z0-9_]", "", "{}_{}{}".format(sectionName, chapterName, split_count)) + ".xhtml"
                            newChapter = epub.EpubHtml(title=chapterName + (str(split_count) if split_count != 1 else ""), file_name=fileName, lang='en')
                            newChapter.content = remove_control_characters(pagePart.prettify())
                            newChapter.add_item(css)
                            book.add_item(newChapter)
                            #print("Piece {} has been added.".format(split_count))
                            
                            if (split_count == 1):
                                prevCategory[1].append(newChapter)
                                #print("Included in the Table of Contents.")
                            spine.append(newChapter)
                            
                            htmlData = htmlData[index:]
                            split_count += 1
                        print("Large chapter. {} parts created.".format(split_count-1))
                    else:
                        fileName = re.sub("[^A-Za-z0-9_]", "", "{}_{}".format(sectionName, chapterName)) + ".xhtml"
                        newChapter = epub.EpubHtml(title=chapterName, file_name=fileName, lang='en')
                        
                        newChapter.content = remove_control_characters(newPageData.prettify()) #No idea what characters these are...
                        newChapter.add_item(css)
                        book.add_item(newChapter)
                        prevCategory[1].append(newChapter)
                        spine.append(newChapter)

if (len(prevCategory) > 1):
    toc.append(prevCategory)
book.toc = toc
book.add_item(epub.EpubNcx())
book.add_item(epub.EpubNav())
book.spine = spine

epub.write_epub("effulgence.epub", book, {})