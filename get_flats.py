#!/usr/bin/python3
# coding: utf-8
from bs4 import BeautifulSoup
import urllib
import urllib.parse as urlparse
import re
import os
import subprocess

def findFileFromURL(chapterURL, where="web_cache"):
    if (where != ""): where = where + "/"
    originalChapterURL = chapterURL
    chapterURL = chapterURL.replace("http://", where).replace("https://", where)
    
    urlSplit = urllib.parse.urlsplit(chapterURL)
    queryParams = urllib.parse.parse_qs(urlSplit.query)
    
    fileName = urlSplit.path.split("/")[-1]
    filePath = ("/").join(urlSplit.path.split("/")[:-1]) + "/"
    
    #print("Looking for: {}".format(chapterURL))
    
    foundFile = None # e.g. "513.html?blah=blah" when found
    if (not os.path.isdir(filePath)):
        print("Couldn't find the directory [{}].".format(filePath))
        print("Chapter URL: {}".format(originalChapterURL))
        print("Where: {}".format(where))
    else:
        for item in os.listdir(filePath):
            itemSplit = urllib.parse.urlsplit(item)
            
            if (itemSplit.path == fileName and os.path.isfile(filePath + item)):
                #File has our filename, it's also a file.
                fileParams = urllib.parse.parse_qs(itemSplit.query)
                hasParams = True
                #print("File: {}".format(item))
                for queryParam in queryParams:
                    if (not queryParam in fileParams or not (queryParams[queryParam] in fileParams[queryParam] or fileParams[queryParam] == queryParams[queryParam])):
                        #It doesn't have the current param
                        hasParams = False
                        #print("File is missing param {} or fileParams[queryParam] ({}) doesn't have queryParams[queryParam] ({})".format(queryParam, fileParams[queryParam] if queryParam in fileParams else "Missing", queryParams[queryParam]))
                for fileParam in fileParams:
                    for fileParamValue in fileParams[fileParam]:
                        if (not fileParam in queryParams or not (fileParamValue in queryParams[fileParam] or queryParams[fileParam] == fileParamValue)):
                            hasParams = False #Some parameters are missing from the input for this to be the valid file.
                            #print("File has a param ({}) not in query - either queryParams[fileParam] ({}) is missing or not the same as the file's ({})".format(fileParam, queryParams[fileParam] if fileParam in queryParams else "Missing", fileParamValue))
                if (hasParams):
                    foundFile = filePath + item
        
    #print("Number of files in {}: {}".format(filePath, len(os.listdir(filePath))))
    if not (foundFile):
        print("Couldn't find file [{}] in [{}] (findFileFromURL(\"{}\", \"{}\"))".format(fileName, filePath, originalChapterURL, where))
        #raise FileNotFoundError("Couldn't find file [{}]".format(chapterURL))
        return None
    else:
        return foundFile

def set_param_in_url(url, *args, **kargs):
    if (not len(args) % 2 == 0):
        raise ValueError("Please give a URL then index,value pairs to replace in the query string.")
    
    urlSplit = urllib.parse.urlsplit(url)
    params = urllib.parse.parse_qs(urlSplit.query)
    
    for i in range(0, int(len(args)/2)):
        params[args[i * 2]] = [args[i * 2 + 1]]
    for karg in kargs:
        params[karg] = [kargs[karg]]
    
    new_urlSplit = urlSplit._replace(query=urllib.parse.urlencode(params, doseq=True))
    return urllib.parse.urlunsplit(new_urlSplit)

def clear_params_in_url(url):
    urlSplit = urllib.parse.urlsplit(url)
    new_urlSplit = urlSplit._replace(query=[])
    return urllib.parse.urlunsplit(new_urlSplit)
    
def set_url_params(url, *args, **kargs):
    return set_param_in_url(clear_params_in_url(url), *args, **kargs)

#Fetch a file from [url] to the directory [where="web_cache"], showing output if [output],
#generating a message if [notify], and waiting for the download to finish if [wait].
def get_file(url, where="web_cache", output=False, notify=False, wait=True, timestamping=True):
    if (not os.path.isdir(where)):
        os.makedirs(where)
    proc = subprocess.Popen("cd \"" + where + "\" && wget --load-cookies ../wget-cookies --save-cookies ../wget-cookies -w 0.5 --no-verbose --force-directories" + (" --timestamping" if timestamping else "") +" \"" + url + "\"" + (" > /dev/null 2>&1" if not output else ""), shell=True)
    if (wait): 
        proc.wait()
        if (not findFileFromURL(url, where=where)):
            #Retry
            proc = subprocess.Popen("cd \"" + where + "\" && wget --load-cookies ../wget-cookies --save-cookies ../wget-cookies -w 0.5 --force-directories" + (" --timestamping" if timestamping else "") +" \"" + url + "\"", shell=True)
            proc.wait()
            if (not findFileFromURL(url, where=where)):
                raise FileNotFoundError("Couldn't download file [{}]".format(url))
    if (notify): print("Got file {}".format(url))

#Convert the URL [haystack] to an internal URL by replacing all instances of the
#[needles=["http://", "https://"]] to [replace="web_cache/"]
def internalise_url(haystack, replace="web_cache/", needles=["http://", "https://"]):
    if (type(needles) is str): needles = [needle]
    for needle in needles:
        haystack = haystack.replace(needle, replace)
    return haystack

def externalise_url(haystack, replace="http://", needles=["web_cache/"]):
    if (type(needles) is str): needles = [needle]
    for needle in needles:
        haystack = haystack.replace(needle, replace)
    return haystack
    
chapterList = [] #[[web_cache/alicorn.../###.html?view=flat, "Ω ⍋ in defiance of the capitol", "outside help"], ...]
with open("web_cache/tocPages.txt", mode="r") as tocs:
    for toc in tocs:
        if (toc == "" or toc == "\n"):
            continue
        
        toc = toc.replace("\n", "")
        
        print("TOC Page: {}".format(toc))
        #Get a nice BeautifulSoup of the TOC's HTML
        soup = BeautifulSoup(open(internalise_url(toc)))
        
        #Get a list of chapter links from each TOC
        sections = soup.select(".entry-content > ol > li")
        for section in sections:
            #Effulgence is different from Incandescence - check for section titles first.
            #Then we can find sub-sections (chapters, if they exist)
            #Else we'll just use the section as a chapter. Hopefully it'll have a link. :(
            
            sectionChapter = False
            chapters = section.select("ol > li > a")
            if (len(chapters) < 1):
                #This section is probably a chapter all by itself.
                chapters = section.select("> a")
                sectionChapter = True
                
            #Find the section title (e.g. when on "1. Chamomile > 1.4. Clannish", finds "Chamomile")
            #(The HTML is horrible, since we also want to include some un-italicised stuff, so we just
            # do everything before a <br> tag)
            sectionTitle = ""
            for word in section.contents:
                haystack = str(word).replace(" ", "")
                if ("<br>" in haystack or "<br/>" in haystack or "<ol>" in haystack or "<ul>" in haystack):
                    break
                sectionTitle += str(word)
            sectionTitle = re.sub(r"\<[\s\S]*?\>", "", sectionTitle).strip()
            
            if (len(chapters) < 1):
                print("Error: Couldn't find any chapters for \"{}\"".format(sectionTitle))
            
            for link in chapters:
                #Find the chapter's URL (it's the link); for dreamwidth, we "flatten" (more later) this and make it use the site's style.
                chapterURL = set_url_params(link["href"], **{"style": "site", "view": "flat"})
                get_file("{}".format(chapterURL), timestamping=False)
                #print("Got file: {}".format(chapterURL))
                
                #Find the chapter's name (e.g. "1. make a wish > 1.1. [] he couldn't have imagined", finds "he couldn't have imagined")
                #(It's the link text)
                
                if (sectionChapter):
                    chapterName = sectionTitle
                else:
                    chapterName = ""
                    for word in link.parent.contents:
                        haystack = str(word).replace(" ", "")
                        if ("<br>" in haystack or "<br/>" in haystack or "<ol>" in haystack or "<ul>" in haystack):
                            break
                        chapterName += str(word)
                    chapterName = re.sub(r"\<[\s\S]*?\>", "", chapterName).strip()
                
                print("Parsed the first page of \"{}\" from \"{}\"".format(chapterName, sectionTitle))
                
                #Add it to the list
                chapterList.append([internalise_url(chapterURL), chapterName, sectionTitle])

#Because dreamwidth is special in how it shows the text, we're flattening stuff and downloading each page of the part.
pageMetas = {} #["alicorn.../###.html?view=flat&style=site"] = [25]
with open("web_cache/flatPageMeta.txt", mode="r") as pageMeta:
    for line in pageMeta:
        if (line == "" or line == "\n"):
            continue
        
        #Get the line's data
        lineAttribs = line.replace("\n", "").split(" # ")
        
        #Set up as: "alicorn.../###.html?view=flat&style=site # 25"
        lineURL = lineAttribs[0]
        lineLastPage = int(lineAttribs[1])
        
        filesExist = True
        if (findFileFromURL("http://" + lineURL)):
            for i in range(1, lineLastPage+1):
                if (not findFileFromURL(set_url_params("http://" + lineURL, **{"page" : i, "style": "site", "view": "flat"}))):
                    filesExist = False
        else:
            filesExist = False
        
        if (filesExist):
            pageMetas[lineURL] = [lineLastPage]
            
        #Since we have all the pages up to and including lineLastPage, we've put it in a list.

#chapterList = [] #[[web_cache/alicorn.../###.html?view=flat&style=site, "Clannish", "Chamomile", PAGECOUNT], ...]

with open("web_cache/flatPageMeta.txt", mode="w") as pageMetaFile:
    print("No comment means \"Appears to have no changes before the last page\"")
    print("\"Length changed\" means \"The number of pages appears to have changed\"")
    print("\"Changed\" means \"The 2nd-last page appears to be different\" (and therefore some previous pages too)")
    for chapter in chapterList:
        chapterURL = chapter[0]
        chapterTitle = chapter[1]
        chapterSection = chapter[2]
        
        with open(chapterURL) as chapterFirstPage:
            soup = BeautifulSoup(chapterFirstPage)
            chapterEntryTitle = soup.find("h3", class_="entry-title").get_text()
            print("Processing: {} - {}".format(chapterSection, chapterEntryTitle))
            
            page_list = soup.find("div", class_="comment-page-list")
            if (page_list):
                page_count = int(re.match(r'Page 1 of ([0-9]+)', page_list.find("p").text).groups()[0])
            else:
                page_count = 1
            chapter.append(page_count)
            
            chapterLoc = chapterURL.replace("web_cache/", "")
            chapterDone = False
            if (chapterLoc in pageMetas):
                if (page_count == pageMetas[chapterLoc][0] and pageMetas[chapterLoc][0] > 1):
                    #We have the same page-count.
                    #Re-download the 2nd last page, check it's the same, then download the last page.
                    chapterPenultURL = set_url_params("http://" + chapterLoc, **{"page" : pageMetas[chapterLoc][0] - 1, "style": "site", "view": "flat"})
                    get_file(chapterPenultURL, where="temp", timestamping=False)
                    webcachePenult = findFileFromURL(chapterPenultURL, where="web_cache/")
                    tempPenult = findFileFromURL(chapterPenultURL, where="temp/")
                    with open(webcachePenult) as f1:
                        with open(tempPenult) as f2:
                            soup1 = BeautifulSoup(f1)
                            soup2 = BeautifulSoup(f2)
                            if (str(soup1.select("div#content")[0]) == str(soup2.select("div#content")[0])):
                                #if (f1.read() == f2.read()):
                                #It's the same!
                                chapterDone = True
                                #Don't bother downloading the whole thing
                                #Just get the latest chapter, since it's possible we have
                                #the right number of pages, and the 2nd last page is the
                                #same, but the most recent page is different.
                                get_file(set_url_params("http://" + chapterLoc, **{"page" : pageMetas[chapterLoc][0], "style": "site", "view": "flat"}), timestamping=False)
                                print("Unchanged.")
                            else:
                                print("Changed.")
                                print("Page {} and page {} are different?".format(webcachePenult, tempPenult))
                                #different therefore download it.
                elif (page_count == pageMetas[chapterLoc][0]):
                    print("This chapter seems to only have one page.")
                else:
                    print("Length changed.")
                    print("Prev page count: {}/{}".format(pageMetas[chapterLoc][0], page_count))
                    #Different count! Re-download them all.
                    #chapterDone = False #still
                    
            
            if (not chapterDone):
                for i in range(1, page_count+1):
                    get_file(set_url_params("http://" + chapterLoc, **{"page" : i, "style": "site", "view": "flat"}), timestamping=False)
                print("Downloaded {} pages.".format(page_count))
            
            pageMetas[chapterLoc] = [page_count]
            pageMetaFile.write("{} # {}\n".format(chapterLoc, str(page_count)))
            pageMetaFile.flush()

#Now let's make the files so parse_all_flats_epub.py knows what it's got to parse.
with open("web_cache/chapterList.txt", mode="w") as chapterListFile:
    with open("web_cache/chapterNames.txt", mode="w") as chapterNamesFile:
        for chapter in chapterList:
            chapterURL = chapter[0]
            chapterTitle = chapter[1]
            chapterSection = chapter[2]
            chapterPageCount = chapter[3]
            
            chapterListFile.write("{}\n".format(chapterURL.replace("web_cache/", "http://")))
            chapterNamesFile.write("{} ~#~ {}\n".format(chapterSection, chapterTitle))

