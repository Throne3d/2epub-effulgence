function get () {
    cd web_cache
    wget --no-verbose --load-cookies ../wget-cookies --save-cookies ../wget-cookies -w 0.5 \
        --force-directories --timestamping -i ../$@
    cd ..
}

[ -e web_cache ] || mkdir web_cache

[ -e web_cache/tocPages.txt ] || echo -e "https://edgeofyourseat.dreamwidth.org/2121.html?style=site" > web_cache/tocPages.txt
get web_cache/tocPages.txt

[ -e web_cache/collectionPages.txt ] || echo -e "Kappa ~#~ http://binary-heat.dreamwidth.org/profile\nAlicorn ~#~ http://belltower.dreamwidth.org/profile\nAestrix ~#~ http://aestrices.dreamwidth.org/profile" > web_cache/collectionPages.txt

[ -e web_cache/flatPageMeta.txt ] || echo "" > web_cache/flatPageMeta.txt
python3 get_flats.py

python3 parse_all_flats_epub.py

